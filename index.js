const _ = require('lodash')
const path = require('path')
const jsonServer = require('json-server')

const port = process.env.PORT || 3333
const dbPath = path.join(__dirname, 'db.json')

const server = jsonServer.create()
const router = jsonServer.router(dbPath)
const middlewares = jsonServer.defaults()

// Set default middlewares
server.use(middlewares)

// Use default routes
server.use(router)

// Start server
server.listen(port, function () {
	console.log('Server running on port ' + port + '...')
})
